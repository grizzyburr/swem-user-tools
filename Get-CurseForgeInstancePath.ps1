function Get-CurseForgeInstancePath {
	$curseforge_config = get-content "$Env:APPDATA\CurseForge\storage.json" | Convertfrom-json
	$minecraft_config = $curseforge_config.'minecraft-settings' | convertfrom-json
	$minecraft_path = $minecraft_config.'minecraftRoot'
	$instance_path = $minecraft_path + '\Instances'
	return $instance_path
}

$a = Get-CurseForgeInstancePath
write-host $a