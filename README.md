# SWEM User Tools

These are the SWEM User Tools. This repo contains various scripts to assist with some troubleshooting tasks.   

## How To Use
You can run any script in this repo by executing the below command in command prompt, replacing `<script>` with the name of the script in the repo.   
```
powershell.exe -c "invoke-webrequest https://gitlab.com/grizzyburr/swem-user-tools/-/raw/main/<script>?ref_type=heads | iex"
```
*Note: You should not normally execute powershell scripts in this way unless you can verify what the script is actually doing (and sometimes not even then). I advise reading the scripts and understanding what they do before you try to run them*   
